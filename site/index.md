@def title = "the dotfiles of the Expanding Man"

~~~
<center>
~~~
$\hbar=c=1$
~~~
</center>
~~~

# about
This site is associated with [my dotfiles repo](https://gitlab.com/ExpandingMan/dotfiles).

Here I will give some information on my linux configuration and its installation.
Additionally, this site serves as a curated list of software and tools which I maintain
for my own reference.  Not everything in these lists is great, though much of it is.  If
it's here it's because I use it, or have used it.

# dotfiles
The configuration itself can be found in
[`etc/`](https://gitlab.com/ExpandingMan/dotfiles/-/tree/master/etc).

I prefer running [Arch linux](https://www.archlinux.org/) or its forks.  Most of this
should work fairly well on most linux distributions.

### window manager
The window manager used is [i3](https://i3wm.org/).  In the future I'd like to upgrade to
[sway](https://swaywm.org/), but as I have multiple machines with nvidia GPU's, this
won't happen for the foreseeable future.  The i3 setup itself is fairly standard and
minimal.  Some "special" workspaces are set up for the browser, steam and games.
With the standard installation you can see a list of i3 keybindings by pressing
`mod+Y`.

A status bar with some useful system information is rendered with
[polybar](https://github.com/polybar/polybar).

### terminal
The terminal used is [alacritty](https://github.com/alacritty/alacritty).

The shell I use is [`fish`](https://fishshell.com/) with [`starship`](https://starship.rs/) prompt.
My `fish` config kept in [this repo](https://gitlab.com/ExpandingMan/fishconfig).  I have the
ability to bootstrap into fish with reasonable defaults for `bash` and `zsh`, see below.

Text editing is done with [neovim](https://github.com/neovim/neovim) with a setup which is written
entirely in Lua and which has [its own repository](https://gitlab.com/ExpandingMan/neovimconfig).

### colors and appearance
I use the [dracula theme](https://github.com/dracula/dracula-theme) for pretty much
everything, including this site.  I landed on this after having used solarized dark for a
while but ultimately being driven near nausea from the dull, faded colors.  I enjoy neon
color schemes, of which dracula is an example, though not necessarily the best.

My use of dracula everywhere may lead you to conclude
that I am in love with this theme.  This is not the case.  However, after multiple
disastrous attempts to construct my own color scheme, I have a deep appreciation for how
incredibly hard it is to create a good color scheme, particularly one you intend to use
for code highlighting.  At some point I'd like to either create or obtain a neon
colorscheme I like better but for now, dracula it is.

I use the [JuliaMono](https://github.com/cormullion/juliamono) font for all monospaced fonts.  This
font has impeccable unicode support and clearly distinguished delimiters.  For non-monospace fonts,
I use [Computer Modern](https://tug.org/FontCatalogue/computermodern/) font (serif where possible)
which is the LaTeX font.

### julia
I make frequent use of my favorite programming language, [Julia](https://julialang.org/).  Julia is
a fast, garbage-collected language with a novel runtime.  It is developed largely with scientific
and numerical computing in mind, and as such takes mathematical abstraction delightfully seriously.
Despite its scientific roots, it is a true general purpose language.  I normally use the Julia REPL
either alongside or within `nvim`, with [Revise.jl](https://github.com/timholy/Revise.jl).

### the Great Shell Bootstrap
I use `fish` which is not installed by default on most systems, but I also `ssh` into other systems
or use docker images relatively frequently.  Not having a properly configured shell can be scary, so
I maintain a configuration for `bash` and `zsh`.  The shell configuration is stratified thusly:
- `.profile` exports important variables that *must* be available to the window manager.
- `.bashrc` contains my primary configuration for `bash` and `zsh`.
- `.zshrc` contains basically nothing not present for `bash`.
- `.config/fish/` contains the fish shell configuration from the
    [fishconfig](https://gitlab.com/ExpandingMan/fishconfig) repo.

Each strata sources the one below it.  Additionally, I use quite a few command line tools written in
rust which are not initially present on most systems, and my configuration ensures they are present
before setting them as default.  Therefore, I can quickly achieve a reasonable setup on most systems
in `bash`.  On Arch I typically install the rust tools via the systme package manager, on other
distros it's usually better to use `rustup` and `cargo`.

## installer

\fig{/assets/installer.gif}

I have written an installer in Julia.  You should enter the installer with `install.sh`.
The installer will launch you into a Julia REPL with various functions available to
facilitate installation of various programs.  When run interactively, various prompts will
be given so that you can safely install new configs with full control over what gets
overwritten.  You can see a list of programs the installer can install and configure with
`install()`, or see the list
[here](https://gitlab.com/ExpandingMan/dotfiles/-/tree/master/Installer/src/programs).

An increasingly large number of programs I make use of are written in rust and are most easily
installed with `cargo install`.  I typically install rust through `pacman`, but for non-Arch it can
be bootstrapped through the installer by installing the `rustup` init script via
`install(Programs.rust)`.

> **NOTE**: The installer is intended for installing configurations and programs that
> some form of special setup.  In cases where the package manager is sufficient for
> installation, it is much preferred.

You can install Julia itself to `/opt/julia` with `sudo ./install.sh julia`
