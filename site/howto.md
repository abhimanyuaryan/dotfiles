Here are some brief instructions for stuff that I can never remember how to do.

## Boot
- You can enable the GRUB boot menu by setting `GRUB_TIMEOUT_STYLE=menu` in
    `/etc/default/grub` and then `update-grub`

## Screen and Power Management
- `xset s $NSECONDS`: Sets the timer for the screen to be deactivated to `$NSECONDS`.

## System Misc
- Install `chrony` to sync the system clock.  In some cases just installing will do it, you can
    start and enable the `systemd` daemon with `systemctl enable chronyd`.

## Console
- `man console_codes`: Describes how to control the console with `stdout`.  You can move
    the cursor around, set colors, stuff like that.  Need for anything TUI based.

## Disks
- `sudo fdisk -l`: Lists mounted disk volumes.  Useful for finding, for example USB flash drives.
- `dd bs=4M if=name_of_iso_file.iso of=/dev/sdX`: Write an ISO file to the drive at `/dev/sdX`.  (Unmount drive with `umount` first.)

## Libraries
- `ldconfig -p`: Lists all libraries in library paths. For example do `ldconfig -p | grep libsdl` to find SDL libraries.
- `ldd`: Lists all the libraries that the binary links to.
- `objdump -p`: Lists all libraries that a binary links to among other things. Note you can also do `ldd` but this may result in code execution.

## Display Servers
- You can start a new X windows session from one of the other terminals (i.e.
    `ctrl-alt-Fn` by doing `startx` from that terminal.
- Wayland is actually a protocol that's implemented by the desktop environments, so there
    is no equivalent to `startx`.  You can try running it from e.g. GNOME by doing
    `gnome-session --session gnome-wayland`.
- Run `xev` to get an interactive description of how inputs are referred to.  Especially
    useful for binding events ot obscure keys.

## Changing GUI Theme in I3
Most programs seem to take their GUI settings from `~/.config/gtk-3.0/settings.ini`.  This settings
here are referencing some stuff in `~/.config` but mostly from `/usr/share/themes`.  This is
expected to contain a bunch of theme directories each of which has a shitload of settings for all
sorts of different GUI programs.

Refer to [dracula-gtk](https://github.com/dracula/gtk) for a comprehensive example of what this
should look like.  My installer clones this directory when doing `install(Programs.gtk)` as `sudo`.

Multiple different GUI programs are used to set GUI settings in different contexts:
- `lxappearance`: seems to set `~/.config/gtk-3.0/settings.ini`.
- `qt5ct`: Settings for stuff that uses Qt I guess.
- `kvantum`: the GUI front-end is `kvantummanager`, this comes form KDE but may set some things,
    not too sure.

Most of these seem to ultimately take the cues from `~/.config/gtk-3.0/settings.ini` so I *think*
that in some cases this is all that's needed.

## All-In-One Scripts
You can write a script in a language other than bash and make it executable.  For example
```julia
#!/bin/bash
#=
exec julia --project --startup-file=no -q --compile=min -O0 "${BASH_SOURCE[0]}" "$@"
=#
println("what up!")
```
gives minimal latency for Julia (short of actually compiling a binary).
