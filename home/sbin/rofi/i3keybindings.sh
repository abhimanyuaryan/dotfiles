#!/bin/bash

if [[ -z "${XDG_CONFIG_HOME}" ]]; then
	CONFIG_HOME=$HOME/.config
else
    CONFIG_HOME=$XDG_CONFIG_HOME
fi

list=$(i3-msg -t get_config \
    | awk -v separator=" " '
$1 == "#" {
    # Strip down the indent characters
    gsub(/^\s+/, "", $0);
    comment = substr($0, 3);
}
match($0, /^[^\s]+mode ".+"/) != 0 {
    mode = pre_mode toupper(substr($2, 2, length($2)-2)) post_mode;
}
match($0, /^}/) != 0 {
    mode = "";
}
$1 == "bindsym" {
    # Strip down the $ character from variables such as $Mod
    gsub(/\$/, "", $2)
    line = pre_keybinding $2 post_keybinding separator comment;
    # Replace $num in the comment by the number found in the keybinding
    if (index(comment, "$num")) {
        gsub(/\$num/, substr($2, match($2, /([[:digit:]]+)$/), length($2)), line);
    }
    print mode line;
}')

echo "$list" | rofi -dmenu \
                    -markup-rows \
                    -i \
                    -p "  Keybindings"
