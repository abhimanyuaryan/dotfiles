#!/usr/bin/env bash

# TODO accept arguments so you can call it from i3 key-binding

rofi_command="rofi -theme volume.rasi -config $HOME/.config/rofi/util_config.rasi"

## Get Volume
MUTE=$(amixer get Master | tail -n 1 | awk -F ' ' '{print $6}' | tr -d '[]%')

active=""
urgent=""
if [[ $MUTE == *"off"* ]]; then
    active="-a 1"
else
    urgent="-u 1"
fi

if [[ $MUTE == *"on"* ]]; then
    VOLUME="$(amixer get Master | tail -n 1 | awk -F ' ' '{print $5}' | tr -d '[]%')%"
else
    VOLUME="muted"
fi

## Icons

ICON_SOUND="🔈"
ICON_UP="◓"
ICON_DOWN="◒"
ICON_MUTED="○"

options="$ICON_UP\n$ICON_MUTED\n$ICON_DOWN"

## Main
chosen="$(echo -e "$options" | $rofi_command -mesg $ICON_SOUND -p "$VOLUME" -dmenu $active $urgent -selected-row 0)"
case $chosen in
    $ICON_UP)
        amixer -Mq set Master,0 5%+ unmute && notify-send -a volume -u low -t 1500 "$ICON_SOUND $ICON_UP"
        ;;
    $ICON_DOWN)
        amixer -Mq set Master,0 5%- unmute && notify-send -a volume -u low -t 1500 "$ICON_SOUND $ICON_DOWN"
        ;;
    $ICON_MUTED)
        amixer -q set Master toggle && notify-send -a volume -u low -t 1500 $ICON_MUTED
        ;;
esac
